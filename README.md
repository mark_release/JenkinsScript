# JenkinsScript

## pretest.groovy
- MR trigger
- config path
    - test_conf.yaml
    - e.g.
    ```
    pretest:
        build_cmd: "sh build/make.sh"
        test_cmd: "sh tests/sanity.sh"
    ```

## daily_test.groovy
- Parameter
    - PROJ_URL:
        - project url path for git clone
    - BRANCH:
        - branch for building and testing
- config path
    - test_conf.yaml
    - e.g.
    ```
    regression:
        build_cmd: "sh build/make.sh"
        test_cmd: "sh tests/regression.sh"
        mailing:
            maillist:
                - "markhuang3310@gmail.com"

    ```

