currentBuild.displayName = "MR-${env.gitlabMergeRequestTitle} by ${env.gitlabUserName}, ${env.gitlabTriggerPhrase}"
updateGitlabCommitStatus name: 'jenkins', state: 'pending'
node(){
    updateGitlabCommitStatus name: 'jenkins', state: 'running'
    try{
        git branch: "${env.gitlabSourceBranch}", credentialsId: 'gitlab_markhuang3310_https', url: "${env.gitlabSourceRepoHttpUrl}"
        def conf = readYaml file: "test_conf.yaml"
        pretestInfo = conf['pretest']
        build_cmd = pretestInfo['build_cmd']
        test_cmd = pretestInfo['test_cmd']
        
        stage("build"){
            sh build_cmd
        }
        stage("test"){
            sh test_cmd
        }
        stage("notify"){
            addGitLabMRComment comment: 'Pre-test: Pass'
        }
        updateGitlabCommitStatus name: 'jenkins', state: 'success'
    }catch(Exception e){
        updateGitlabCommitStatus name: 'jenkins', state: 'failed'
        addGitLabMRComment comment: 'Pre-test: Fail'
        error("Test fail")
    }
}
