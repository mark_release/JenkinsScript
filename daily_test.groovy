node(){
    try{
        git branch: "$BRANCH", credentialsId: 'gitlab_markhuang3310_https', url: "$PROJ_URL"

        conf = readYaml file: "test_conf.yaml"
        regressionInfo = conf['regression']
        build_cmd = regressionInfo['build_cmd']
        test_cmd = regressionInfo['test_cmd']
        mail_list = regressionInfo['mailing']['maillist']
        mail_to = mail_list.join(",")

        stage("build"){
            sh build_cmd
        }
        stage("test"){
            sh test_cmd
        }
    }catch(Exception e){
        currentBuild.result = 'FAILURE'
        throw e
    }finally{
        stage("mailing"){
            emailext body: '''${SCRIPT, template="groovy-html.template"}''',
            to: mail_to,
            subject: "${env.JOB_NAME}"
        }
    }
}
